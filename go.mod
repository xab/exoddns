module gitlab.com/xab/exoddns

go 1.17

require github.com/exoscale/egoscale v1.19.0

require github.com/gofrs/uuid v3.2.0+incompatible // indirect
