package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	//	"fmt"
	"github.com/exoscale/egoscale"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strconv"
)

// Config default config
type Config struct {
	Key          string `json:"EXO_KEY"`
	Secret       string `json:"EXO_SECRET"`
	Domain       string
	RecordName   string
	RecordType   string
	CreateRecord bool
	RecordTTL    string
}

const defaultTTL string = "60"
const exoDNSApi = "https://api.exoscale.com/dns"

var ipifyURL = map[string]string{
	"A":    "https://api.ipify.org/",
	"AAAA": "https://api64.ipify.org/"}

// readConfig reads config from json
func readConfig(filename string) (*Config, error) {
	// initialize conf with default values.
	conf := &Config{RecordTTL: defaultTTL}

	b, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal("Cannot read" + filename)
		return nil, err
	}

	if err = json.Unmarshal(b, &conf); err != nil {
		log.Fatal("cannot unmarshall json")
		return nil, err
	}
	return conf, nil
}

//get my ip from ipify
func getMyIP(recordType string) (string, error) {

	res, err := http.Get(ipifyURL[recordType])
	if err != nil {
		log.Fatal("Cannot get IP from: " + ipifyURL[recordType])
	}

	ip, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal("Cannot read the body")
	}

	ipStr := string(ip)

	if net.ParseIP(ipStr) == nil {
		log.Fatalf("%s is not a valid IP\n", ipStr)
	}

	return ipStr, nil
}

func getDomainID(ctx context.Context, client *egoscale.Client, domain string) (int64, error) {

	domainResp, err := client.GetDomain(ctx, domain)
	if err != nil {
		return 0, errors.New("Cannot get domain ID")

	}

	return domainResp.ID, nil
}

func main() {
	var configFile = flag.String(
		"configFile",
		"./config.json",
		"Configuration file location")
	flag.Parse()
	jsonConf, err := readConfig(*configFile)
	if err != nil {
		log.Fatal(err)
	}

	myIP, err := getMyIP(jsonConf.RecordType)
	if err != nil {
		log.Fatal(err)
	}

	client := egoscale.NewClient(
		exoDNSApi,
		jsonConf.Key,
		jsonConf.Secret)
	ctx := context.TODO()

	domainID, err := getDomainID(ctx, client, jsonConf.Domain)
	if err != nil {
		log.Fatal(err)
	}

	record, err := client.GetRecordsWithFilters(
		ctx,
		jsonConf.Domain,
		jsonConf.RecordName,
		jsonConf.RecordType)
	if err != nil {
		log.Fatal(err)
	}

	ttl, _ := strconv.Atoi(jsonConf.RecordTTL)

	if len(record) == 0 {
		if jsonConf.CreateRecord {
			//check if record already exists
			respDNS, err := client.CreateRecord(
				ctx,
				jsonConf.Domain,
				egoscale.DNSRecord{
					DomainID:   domainID,
					Name:       jsonConf.RecordName,
					TTL:        ttl,
					RecordType: jsonConf.RecordType,
					Content:    myIP})
			if err != nil {
				log.Fatal(err)
			}
			log.Println("Record " + jsonConf.RecordName + " created successfuly with " + respDNS.Content)
		} else {
			log.Fatal("Record " + jsonConf.RecordName + " doesn't exists")
		}

	} else { //update record
		if myIP != record[0].Content {
			respDNS, err := client.UpdateRecord(
				ctx,
				jsonConf.Domain,
				egoscale.UpdateDNSRecord{
					ID:         record[0].ID,
					DomainID:   domainID,
					Name:       jsonConf.RecordName,
					TTL:        ttl,
					RecordType: jsonConf.RecordType,
					Content:    myIP})
			if err != nil {
				log.Fatal(err)
			}
			log.Println("Record " + jsonConf.RecordName + " updated successfuly with " + respDNS.Content)
		} else {
			log.Printf("Record %s matches current IP(%s), Noop\n", jsonConf.RecordName, myIP)
		}
	}

}
