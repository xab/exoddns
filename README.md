# Exoddns

Dynamic DNS client using [Exoscale DNS API](https://community.exoscale.com/documentation/dns/api-examples/) through [Egoscale](https://github.com/exoscale/egoscale/).

## Description

Exoddns is a client to update **A** or **AAAA** DNS records of a zone hosted at [Exoscale](https://www.exoscale.com/dns/).

It will retreive the IP (v4 or v6) of the host is running on by querying [ipify](https://www.ipify.org/).

## Configuration

You need an [IAM Key](https://community.exoscale.com/documentation/iam/) with correct permissions.

    /exoddns --help
    Usage of ./exoddns:
      -configFile string
    	    Configuration file location (default "./config.json")


By default it will look in ./config.json with the following structure

    {
      "EXO_KEY": "mykey",
      "EXO_SECRET": "myscret",
      "domain": "example.com",
      "recordName": "home",
      "recordType": "A",  # or AAAA
      "recordTTL" : "300", # is optional
      "createRecord": true
    } 
