package main

import "testing"
import "net"

func TestGetMyIPv4(t *testing.T) {
	got, err := getMyIP("A")
	want := net.ParseIP(got)

	if want == nil || err != nil {
		t.Fatalf("Need an IPv4 address")
	}
}

func TestGetMyIPv6(t *testing.T) {
	got, err := getMyIP("AAAA")
	want := net.ParseIP(got)

	if want == nil || err != nil {
		t.Fatalf("Need an IPv6 address")
	}
}
